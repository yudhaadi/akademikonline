-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11 Jan 2020 pada 18.00
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akademik_uhamka_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `username` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `nama`, `email`, `password`, `role_id`, `is_active`, `username`) VALUES
(1, 'Administrator Uhamka', 'admin@uhamka.ac.id', 'admin', 1, 1, 'admin'),
(2, 'hendss', 'hends@gmail.com', '', 1, 0, 'hendrawans'),
(3, 'testuseradm', 'testuser@gmail.com', '12345', 1, 0, 'testuser123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_fakultas`
--

CREATE TABLE `data_fakultas` (
  `id_fakultas` int(11) NOT NULL,
  `fakultas` varchar(30) NOT NULL,
  `jml_prodi` varchar(30) NOT NULL,
  `jml_mhs_fakultas` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_fakultas`
--

INSERT INTO `data_fakultas` (`id_fakultas`, `fakultas`, `jml_prodi`, `jml_mhs_fakultas`) VALUES
(1, 'Fakultas Teknik', '3', '800'),
(2, 'Kedokoteran', '3', '500'),
(3, 'Ekonomi Bisnis', '4', '1000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_prodi`
--

CREATE TABLE `data_prodi` (
  `id_fakultas` int(11) NOT NULL,
  `kd_prodi` varchar(20) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `jumlah_mhs` varchar(255) NOT NULL,
  `akreditasi` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_prodi`
--

INSERT INTO `data_prodi` (`id_fakultas`, `kd_prodi`, `prodi`, `jumlah_mhs`, `akreditasi`) VALUES
(1, 'FT_TI_01', 'Teknik Informatika', '450', 'A'),
(1, 'FT_TM_02', 'Teknik Mesin', '200', 'A'),
(1, 'FT_TE_03', 'Teknik Elektro', '200', 'A'),
(2, 'FK_PK_01', 'Pendidikan Kedokteran', '100', 'B'),
(2, 'FK_TK_02', 'VOKASI TEKNIK KARDIOVASKULER (D3)', '100', 'A'),
(3, 'FE_MN_01', 'MANAJEMEN', '200', 'A'),
(3, 'FE_AK_S1_01', 'AKUNTANSI (S1)', '300', 'A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `kd_dosen` char(5) NOT NULL,
  `kd_jurusan` varchar(11) NOT NULL,
  `nidn` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`kd_dosen`, `kd_jurusan`, `nidn`, `nama`, `alamat`, `email`, `jenis_kelamin`, `username`, `password`, `role_id`) VALUES
('dk_02', '', 213123, 'Bujank', 'jl.birus', 'bujank@gmail.com', 'perempuan', 'fathana1', '123456', 2),
('kd_01', '', 1212123, 'Yudhas', 'Jl.mawar', 'yudha@uhamka.ac.ids', 'lakis', 'yudha1', '123456', 2),
('kd_03', 'TI_01', 12121232, 'Kamu', 'jl.kenangan', 'kamu@gmail.com', 'perempuan', 'kamu1', '1234567', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(10) NOT NULL,
  `nama_dosen` char(30) NOT NULL,
  `nama_mk` char(30) NOT NULL,
  `kd_mk` varchar(30) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `jam` char(30) NOT NULL,
  `ruang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `nama_dosen`, `nama_mk`, `kd_mk`, `hari`, `jam`, `ruang`) VALUES
(1, 'Yudhas', 'Ilmu Sosial dan Budaya Dasar', '03015003', 'senin', '03:00 - 05:00', 'ft305'),
(2, 'Kamu', 'Aqidah', '', 'jumat', '01:00 - 03:00', 'ft503');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` int(11) NOT NULL,
  `nim` bigint(20) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `tempat_lahir` varchar(10) NOT NULL,
  `kewarganegaraan` varchar(30) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(128) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `qr_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mhs`, `nim`, `nama`, `alamat`, `no_telp`, `jenis_kelamin`, `tempat_lahir`, `kewarganegaraan`, `agama`, `tgl_lahir`, `email`, `username`, `password`, `role_id`, `qr_code`) VALUES
(42, 1703015032, 'Yudha Adi Hendrawan Prakoso', 'Jl. Kelapa kuning III Blok I no.12', '083892903537', 'laki - laki', 'jakarta', 'Indonesia', 'Islam', '1999-07-19', 'yudhaadi@uhamka.ac.id', 'yudha1', '12345', 3, 'assets/images/qrcode/1703015032.png'),
(43, 1703015031, 'Prakoso Hendrawan', 'Jl.Biru kuning hijau blok IV', '083892903537', 'laki - laki', 'singapore', 'singapore', 'Islam', '1999-07-19', 'hendrawanPrakoso@gmail.com', 'hendrawan', '1234567', 3, 'assets/images/qrcode/1703015031.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mata_kuliah`
--

CREATE TABLE `mata_kuliah` (
  `kd_mk` char(10) NOT NULL,
  `kd_prodi` varchar(20) NOT NULL,
  `matakuliah` varchar(30) NOT NULL,
  `sks` int(11) NOT NULL,
  `semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mata_kuliah`
--

INSERT INTO `mata_kuliah` (`kd_mk`, `kd_prodi`, `matakuliah`, `sks`, `semester`) VALUES
('03015001', 'FT_TI_01', 'Pendidikan Agama', 3, 1),
('03015002', 'FT_TI_01', 'Kewarganegaraan', 2, 1),
('03015003', 'FT_TI_01', 'Ilmu Sosial dan Budaya Dasar', 2, 1),
('03015004', 'FT_TI_01', 'Aqidah', 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` bigint(30) NOT NULL,
  `nim` bigint(20) NOT NULL,
  `kd_mk` char(10) NOT NULL,
  `dosen_pengampu` char(30) NOT NULL,
  `matakuliah` varchar(30) DEFAULT NULL,
  `tugas` int(11) NOT NULL,
  `ke_aktifan` int(11) NOT NULL,
  `uts` int(11) NOT NULL,
  `uas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `nim`, `kd_mk`, `dosen_pengampu`, `matakuliah`, `tugas`, `ke_aktifan`, `uts`, `uas`) VALUES
(1, 1703015030, '03015003', 'Yudhas', 'Ilmu Sosial dan Budaya Dasar', 90, 90, 100, 90),
(2, 1703015123, '03015001', 'Yudhas', 'Aqidah', 100, 100, 100, 100);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_mk`
--

CREATE TABLE `paket_mk` (
  `id_paket` int(11) NOT NULL,
  `kd_paket` varchar(30) DEFAULT NULL,
  `jmlh_mk` int(11) DEFAULT NULL,
  `paket_smster` varchar(10) DEFAULT NULL,
  `tot_sks` int(11) DEFAULT NULL,
  `kd_prodi` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `paket_mk`
--

INSERT INTO `paket_mk` (`id_paket`, `kd_paket`, `jmlh_mk`, `paket_smster`, `tot_sks`, `kd_prodi`) VALUES
(1, 'SMT_1_A', 15, '1', 24, 'FT_TI_01'),
(2, 'SMT_1_B', 12, '1', 24, 'FT_TI_01'),
(3, 'SMT_2_A', 12, '2', 22, 'FT_TI_01'),
(4, 'SMT_2_B', 12, '2', 21, 'FT_TI_01'),
(6, 'SMT_2_C', 11, '2', 21, 'FT_TM_02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role_id`, `role`) VALUES
(1, 1, 'Administrator'),
(2, 2, 'Dosen'),
(3, 3, 'Mahasiswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_fakultas`
--
ALTER TABLE `data_fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`kd_dosen`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`) USING BTREE;

--
-- Indexes for table `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD PRIMARY KEY (`kd_mk`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `paket_mk`
--
ALTER TABLE `paket_mk`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_fakultas`
--
ALTER TABLE `data_fakultas`
  MODIFY `id_fakultas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mhs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
