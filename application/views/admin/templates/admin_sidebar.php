<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">UHAMKA AKADEMIK</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">

            <li class="menu-header">Dashboard</li>
            <li class="nav-item <?php echo (current_url() == base_url() . 'Admin/index') ? 'active' : '' ?>">
                <a href="<?php echo base_url('Admin/index') ?>" class=" nav-item <?php echo (current_url() == base_url() . 'Admin/index') ? 'active' : '' ?>"><i class="fas fa-home-lg"></i><span>Dashboard</span></a>

            </li>


            <li class="menu-header">Master Data</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataAdmin') ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-user-edit"></i>
                    <span>Edit User</span></a>
                <ul class="dropdown-menu ">
                    <li class="<?php echo (current_url() == base_url() . 'Admin/tampildataAdmin') ? 'active' : '' ?>"><a href="<?php echo base_url('Admin/tampildataAdmin') ?>">Admin</a></li>
                </ul>
            </li>

            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataFakultas') ? 'active' : ''  ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-university"></i>
                    <span>Data Fakultas</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == base_url() . 'Admin/tampildataFakultas') ? 'active' : '' ?>"><a href="<?php echo base_url('Admin/tampildataFakultas') ?>">Edit Fakultas</a></li>

                </ul>
            </li>
            <!-- <li><a class="nav-link" href="blank.html"><i class="far fa-square"></i> <span>Blank Page</span></a></li> -->



            <li class="menu-header">Matakuliah Management</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataMk' || current_url() == base_url() . 'Admin/tampildataPaketMk') ? 'active' : ''  ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"> <i class="fas fa-books"></i>
                    <span>Data Matakuliah</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == 'Admin/tampildataMk') ? "active" : "" ?>"><a href="<?php echo base_url('Admin/tampildataMk') ?>">Edit Matakuliah</a></li>
                    <li class="<?php echo (current_url() == 'Admin/tampildataPaketMk') ? "active" : "" ?>"><a href="<?php echo base_url('Admin/tampildataPaketMk') ?>">Paket MK</a></li>
                    <!-- <li><a class="nav-link" href="layout-transparent.html">Transparent Sidebar</a></li>
                    <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li> -->
                </ul>
            </li>

            <li class="menu-header">Mahasiswa Management</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataMahasiswa') ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"> <i class="fas fa-graduation-cap"></i>
                    <span>Data Mahasiswa</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == base_url() . 'Admin/tampildataMahasiswa') ? 'active' : '' ?>">
                        <a class="nav-link" href="<?php echo base_url('Admin/tampildataMahasiswa') ?>">Edit Mahasiswa</a></li>

                </ul>
            </li>

            <li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataNilai') ? 'active' : ''  ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="far fa-book-user"></i>
                    <span>Data Nilai</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == 'Admin/tampildataNilai') ? "active" : "" ?>"><a href="<?php echo base_url('Admin/tampildataNilai') ?>">Lihat Nilai</a></li>
                    <!-- <li><a class="nav-link" href="layout-transparent.html">Edit Nilai</a></li> -->
                </ul>

            </li>

            <!-- <li>
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-book"></i>
                    <span>Data Absensi</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="layout-default.html">Edit Absensi</a></li>

                </ul>

            </li> -->


            <li class="menu-header">Dosen Management</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Admin/tampildataDosen') ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-book"></i>
                    <span>Data Dosen</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == base_url() . 'Admin/tampildataDosen') ? 'active' : '' ?>"><a href="<?php echo base_url('Admin/tampildataDosen') ?>">Edit Dosen</a></l>
                </ul>
            </li>



        </ul>

    </aside>
</div>