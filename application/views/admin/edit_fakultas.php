<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('templates/dashboard_navbar');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>

<body>
    <div id="app">
        <div class="main-wrapper">

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>Data Fakultas</h1>
                        <div class="section-header-breadcrumb">
                            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                            <div class="breadcrumb-item"><a href="#">Modules</a></div>
                            <div class="breadcrumb-item">DataTables</div>
                        </div>
                    </div>

                    <!-- <div class="section-body">
                        <h2 class="section-title">DataTables</h2>
                        <p class="section-lead">
                            We use 'DataTables' made by @SpryMedia. You can check the full documentation <a href="https://datatables.net/">here</a>.
                        </p> -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4>Data Fakultas</h4> -->

                                </div>
                                <div class="card-body">
                                    <!-- <form id="formcari"> -->
                                    <div class="form-group">
                                        <label>Pilih Fakultas</label>
                                        <select class="form-control" name="fakultas" id="fakultas">
                                            <?php foreach ($data_fakultas as $fak) { ?>

                                                <option value="<?= $fak['id_fakultas'] ?>" class="<?php echo (current_url() == base_url() . 'Admin/tampildataFakultas') ? 'active' : '' ?>"><?= $fak['fakultas'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="cari">Cari</button>
                                    </div>

                                    <!-- </form> -->

                                    <div class="table-responsive">
                                        <?php if (isset($_GET['idfakultas'])) { ?>
                                            <table class="table table-striped" id="table-1">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No. </th>
                                                        <th>Kode Prodi</th>
                                                        <th>Id Fakultas</th>
                                                        <th>Prodi</th>
                                                        <th>Jumlah Mahasiswa</th>
                                                        <th>Akreditasi</th>
                                                        <th>Aksi</th>

                                                        <!-- <th>Status</th>
                                                        <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php $no = 1;
                                                    foreach ($data_prodi as $row) { ?>


                                                        <tr>
                                                            <td class="text-center" width="10%"><?php echo $no++ ?></td>

                                                            <td class="font-w600"><?php echo $row['kd_prodi'] ?></td>
                                                            <td class="font-w600"><?php echo $row['id_fakultas'] ?></td>
                                                            <td class="font-w600"><?php echo $row['prodi'] ?></td>
                                                            <td class="font-w600"><?php echo $row['jumlah_mhs'] ?></td>
                                                            <td class="font-w600"><?php echo $row['akreditasi'] ?></td>

                                                            <td><a id="editBtn" href="#" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                                    <i class=" far fa-edit"></i> Edit
                                                                </a>

                                                                <a href="<?= base_url('Admin/hapusProdi/' . $row['id_fakultas']) ?>" class="hapus">
                                                                    <button class="btn btn-icon icon-left btn-danger" type="button" data-toggle="tooltip"><i class="fas fa-times"></i>Hapus
                                                                    </button>
                                                                </a>
                                                            </td>

                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- </div> -->
                </section>
            </div>

        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/stisla.js"></script>



    <!-- JS Libraies -->
    <script src="<?php echo base_url('node_modules') ?>/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('node_modules') ?>/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url('node_modules') ?>/datatables.net-select-bs4/js/select.bootstrap4.min.js"></script>

    <!-- Template JS File -->
    <script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/custom.js"></script>

    <!-- Page Specific JS File -->
    <script src="<?php echo base_url('assets') ?>/js/page/modules-datatables.js"></script>

    <script>
        $("#table-1").dataTable();


        $('#cari').on('click', function(e) {
            // $('#formcari').submit(function(e)){

            var idfak = $('#fakultas').val()
            window.location.href = "<?= base_url() . 'Admin/tampildataFakultas?idfakultas=' ?>" + idfak;



        })
    </script>

    <?php

    $this->load->view('templates/dashboard_footer');

    ?>