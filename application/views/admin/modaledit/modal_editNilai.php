<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>



<div class="modal fade" tabindex="-1" role="dialog" id="modal_edit_nilai">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="<?php echo base_url('Admin/updateNilai') ?>">

                <input type="hidden" name="nim" id="nim">

                <div class="modal-body">
                    <div class="form-group">
                        <label>Nilai Tugas</label>
                        <input type="text" class="form-control" name="tugas" id="tugas">
                    </div>
                    <div class="form-group">
                        <label>Nilai Ke Aktifan</label>
                        <input type="text" class="form-control" name="ke_aktifan" id="ke_aktifan">
                    </div>
                    <div class="form-group">
                        <label>Nilai UTS</label>
                        <input type="text" class="form-control" name="uts" id="uts">
                    </div>

                    <div class="form-group">
                        <label>Nilai UAS</label>
                        <input type="text" class="form-control" name="uas" id="uas">
                    </div>

                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>

                </div>
            </form>
        </div>
    </div>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/stisla.js"></script>

<!-- JS Libraies -->
<script src="<?php echo base_url('node_modules') ?>/prismjs/prism.js"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>
<script src="<?php echo base_url('assets') ?>/js/custom.js"></script>

<!-- Page Specific JS File -->
<script src="<?php echo base_url('assets') ?>/js/page/bootstrap-modal.js"></script>