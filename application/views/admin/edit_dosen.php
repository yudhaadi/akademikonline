<?php
$this->load->view('templates/dashboard_header');
?>


<?php
$this->load->view('templates/dashboard_navbar');
?>


<?php
$this->load->view('admin/templates/admin_sidebar');
?>


<style>
    .modal-backdrop {
        display: none;
    }
</style>

<?php

include 'modaledit/modal_editDosen.php';
?>


<?php

include 'modaltambah/modal_tambahDosen.php';

?>

<body>
    <div id="app">
        <div class="main-wrapper">

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>Data Dosen</h1>
                        <div class="section-header-breadcrumb">
                            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                            <div class="breadcrumb-item"><a href="#">Modules</a></div>
                            <div class="breadcrumb-item">DataTables</div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4>Data Dosen</h4> -->
                                    <p class="text-muted font-13 m-b-30" style="position: absolute; right:16px">
                                        <button id="addBtn" style="width:133px;" class="btn btn-success icon-left btn-block" data-toggle="modal" data-target="#tambah_Modal_Dsn"> <i class="fas fa-plus-circle"></i> Tambah Data </button>
                                    </p>

                                </div>
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <?php if (!empty($dosen)) { ?>
                                            <table class="table table-striped" id="table-1">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No. </th>
                                                        <th>Nama Dosen</th>
                                                        <!-- <th>Kode Dosen</th>
                                                        <th>Kode Jurusan</th> -->
                                                        <th>Nidn</th>
                                                        <th>Email</th>
                                                        <th>Alamat</th>
                                                        <th>Jenis Kelamin</th>
                                                        <th>Aksi</th>

                                                        <!-- <th>Status</th>
                                                        <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1;
                                                    foreach ($dosen as $row) { ?>
                                                        <tr>
                                                            <td class="text-center" width="10%"><?php echo $no++ ?></td>
                                                            <td class="font-w600"><?php echo $row['nama'] ?></td>
                                                            <td class="font-w600"><?php echo $row['nidn'] ?></td>
                                                            <td class="font-w600"><?php echo $row['email'] ?></td>
                                                            <td class="font-w600"><?php echo $row['alamat'] ?></td>
                                                            <td class="font-w600"><?php echo $row['jenis_kelamin'] ?></td>
                                                            <td><a id="editBtn" href="#" data-id="<?= $row['kd_dosen'] ?>" data-nama="<?= $row['nama'] ?>" data-nidn="<?= $row['nidn'] ?>" data-email="<?= $row['email'] ?>" data-alamat="<?= $row['alamat'] ?>" data-jenis_kelamin="<?= $row['jenis_kelamin'] ?>" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal" style="border-radius: 30px">
                                                                    <i class=" far fa-edit"></i> Edit
                                                                </a>
                                                                <!-- <a href="#" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Hapus</a> -->

                                                                <a href="<?= base_url('Admin/hapusDosen/' . $row['kd_dosen']) ?>" class="hapus">
                                                                    <button class="btn btn-icon icon-left btn-danger" type="button" data-toggle="tooltip" style="border-radius: 30px"><i class="fas fa-times"></i>Hapus
                                                                    </button>
                                                                </a>



                                                            </td>



                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Advanced Table</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="table-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                                                            <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                                        </div>
                                                    </th>
                                                    <th class="text-center">No. </th>
                                                    <th>Nama Dosen</th>
                                                    <th>Nidn</th>
                                                    <th>Email</th>
                                                    <th>Alamat</th>
                                                    <th>Jenis Kelamin</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    <?php } ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- </div> -->
                </section>
            </div>

        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/stisla.js"></script>



    <!-- JS Libraies -->
    <script src="<?php echo base_url('node_modules') ?>/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('node_modules') ?>/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url('node_modules') ?>/datatables.net-select-bs4/js/select.bootstrap4.min.js"></script>

    <!-- Template JS File -->
    <script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/custom.js"></script>

    <!-- Page Specific JS File -->
    <script src="<?php echo base_url('assets') ?>/js/page/modules-datatables.js"></script>




    <script type="text/javascript">
        $("#table-1").dataTable();

        $('#exampleModal').on('show.bs.modal', function(e) {

            var div = $(e.relatedTarget);

            var id = div.data('id');
            var modal = $(this);
            modal.find('#id').attr("value", id);
        })


        // swal hapus
        $('.hapus').on("click", function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            Swal.fire({
                title: 'Anda Yakin?',
                text: "Data tidak bisa di kembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Data Berhasil Dihapus.',
                        'success'
                    ).then(() => {
                        window.location.href = url
                    });
                }
            })
        });


        // editmethodd
        $('#exampleModal').on('show.bs.modal', function(e) {

            var div = $(e.relatedTarget);

            var id = div.data('id');
            var nama = div.data('nama');
            var nidn = div.data('nidn');
            var email = div.data('email');
            var alamat = div.data('alamat');
            var jenis_kelamin = div.data('jenis_kelamin');
            var modal = $(this);
            modal.find('#kd_dosen').attr("value", id);
            modal.find('#nama').attr("value", nama);
            modal.find('#nidn').attr("value", nidn);
            modal.find('#email').attr("value", email);
            modal.find('#alamat').attr("value", alamat);
            modal.find('#jenis_kelamin').attr("value", jenis_kelamin);

        });
    </script>

    <?php

    $this->load->view('templates/dashboard_footer');

    ?>