<?php



defined('BASEPATH') or exit('No direct script access allowed');

class m_dosen extends CI_Model
{


    var $skrg;

    public function getDosen()
    {
        $query = $this->db->get('dosen');
        return $query->result_array();
    }

    public function hitung_matkuldos()
    {
        $skrg = $this->session->userdata('nama');
        $this->db->where('nama_dosen', $skrg);
        $query = $this->db->get('jadwal');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function hitung_mahasiswados()
    {
        $skrg = $this->session->userdata('nama');
        $this->db->where('dosen_pengampu', $skrg);
        $query = $this->db->get('nilai');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function show_Mkdos()
    {
        $skrg = $this->session->userdata('nama');
        $this->db->where('nama_dosen', $skrg);
        $query = $this->db->query("SELECT * FROM jadwal WHERE nama_dosen = '$skrg'");
        return $query->result_array();
    }

    public function show_nilaidos()
    {
        $skrg = $this->session->userdata('nama');
        $this->db->where('nama_dosen', $skrg);
        $query = $this->db->query("SELECT * FROM nilai WHERE dosen_pengampu = '$skrg'");
        return $query->result_array();
    }

    public function update_Mkdos($data, $id)
    {

        $this->db->where('id_nilai', $id);
        $this->db->update('nilai', $data);
        return TRUE;
    }
}
