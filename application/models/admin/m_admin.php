<?php



defined('BASEPATH') or exit('No direct script access allowed');

class m_admin extends CI_Model
{


    public function show_Admin()
    {


        $query = $this->db->query("SELECT * FROM admin");


        return $query->result_array();
    }



    public function show_Dosen()
    {


        $query = $this->db->query("SELECT * FROM dosen");
        return $query->result_array();
    }


    public function show_Mahasiswa()
    {


        $query = $this->db->query("SELECT * FROM mahasiswa");
        return $query->result_array();
    }

    public function show_FakTeknik()
    {


        $query = $this->db->query("SELECT * FROM fakultas_teknik");
        return $query->result_array();
    }

    public function show_Prodi($id)
    {
        $this->db->where('id_fakultas', $id);
        $query =  $this->db->get('data_prodi');
        return $query->result_array();
    }

    public function show_Fakultas()
    {
        $query = $this->db->query("SELECT * FROM data_fakultas");
        return $query->result_array();
    }

    public function show_Mk()
    {

        $query = $this->db->query("SELECT * FROM mata_kuliah");
        return $query->result_array();
    }

    public function show_Nilai()
    {

        $query = $this->db->query("SELECT * FROM nilai");
        return $query->result_array();
    }

    public function show_PaketMk()
    {

        $query = $this->db->query("SELECT * FROM paket_mk");
        return $query->result_array();
    }


    // update
    public function update_admin($data, $id)
    {

        $this->db->where('id', $id);
        $this->db->update('admin', $data);
        return TRUE;
    }

    public function update_dosen($data, $id)
    {

        $this->db->where('kd_dosen', $id);
        $this->db->update('dosen', $data);
        return TRUE;
    }
    public function update_Mhs($data, $id)
    {

        $this->db->where('id_mhs', $id);
        $this->db->update('mahasiswa', $data);
        return TRUE;
    }
    public function update_Mk($data, $id)
    {

        $this->db->where('kd_mk', $id);
        $this->db->update('mata_kuliah', $data);
        return TRUE;
    }

    public function update_Paket($data, $id)
    {

        $this->db->where('id_paket', $id);
        $this->db->update('paket_mk', $data);
        return TRUE;
    }

    public function update_Nilai($data, $id)
    {

        $this->db->where('nim', $id);
        $this->db->update('nilai', $data);
        return TRUE;
    }




    // delete
    public function deleteAdmin($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('admin');
        return TRUE;
    }

    public function deleteDosen($id)
    {
        $this->db->where('kd_dosen', $id);
        $this->db->delete('dosen');
        return TRUE;
    }

    public function deleteMhs($id)
    {
        $this->db->where('id_mhs', $id);
        $this->db->delete('mahasiswa');
        return TRUE;
    }

    public function deleteMk($id)
    {
        $this->db->where('kd_mk', $id);
        $this->db->delete('mata_kuliah');
        return TRUE;
    }

    public function deleteNilai($id)
    {
        $this->db->where('kode_mk', $id);
        $this->db->delete('nilai');
        return TRUE;
    }

    public function deleteProdi($id)
    {
        $this->db->where('id_fakultas', $id);
        $this->db->delete('data_prodi');
        return TRUE;
    }

    public function deletePaket($id)
    {
        $this->db->where('id_paket', $id);
        $this->db->delete('paket_mk');
        return TRUE;
    }


    // create
    public function createAdmin($data)
    {
        $this->db->insert('admin', $data);
        return TRUE;
    }


    public function createDosen($data)
    {
        $this->db->insert('dosen', $data);
        return TRUE;
    }

    public function createMhs($data)
    {
        $this->db->insert('mahasiswa', $data);
        return TRUE;
    }

    public function createMk($data)
    {
        $this->db->insert('mata_kuliah', $data);
        return TRUE;
    }

    public function createPaket($data)
    {
        $this->db->insert('paket_mk', $data);
        return TRUE;
    }


    // count
    public function hitungJumlahAssetMhs()
    {
        $query = $this->db->get('mahasiswa');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function hitungJumlahAssetBuku()
    {
        $query = $this->db->get('mata_kuliah');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function hitungJumlahAssetAdmin()
    {
        $query = $this->db->get('admin');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function hitungJumlahAssetDosen()
    {
        $query = $this->db->get('dosen');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function pencarianFakultas($data)
    {

        $this->db->where("tingkat", $data);
        // return $this->db->get("aps");
    }
}
