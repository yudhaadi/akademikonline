<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/m_admin', 'm_admin');
    }


    public function index()
    {
        $data['title'] = 'Administrators';

        // $data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('username')])->row_array();
        $data['admin'] = $this->m_admin->show_Admin();
        $data['total_asset'] = $this->m_admin->hitungJumlahAssetMhs();
        $data['total_admin'] = $this->m_admin->hitungJumlahAssetAdmin();
        $data['total_dosen'] = $this->m_admin->hitungJumlahAssetDosen();
        $data['total_buku'] = $this->m_admin->hitungJumlahAssetBuku();

        $this->load->view('admin/dashboard_admin', $data);
    }

    // public function seputarUhamka()
    // {

    //     $data['title'] = 'All Informasi';
    //     $data['total_asset'] = $this->m_admin->hitungJumlahAssetMhs();
    //     $data['total_admin'] = $this->m_admin->hitungJumlahAssetMhs();
    //     $data['total_buku'] = $this->m_admin->hitungJumlahAssetBuku();



    //     $this->load->view('admin/dashboard_admin', $data);
    // }


    // public function tampilasset()
    // {


    //     $data['total_asset'] = $this->m_admin->hitungJumlahAssetMhs();

    //     $this->load->view('admin/dashboard_info', $data);
    // }




    public function tampildataAdmin()
    {

        $data['title'] = 'Edit Admin';

        $data['admin'] = $this->m_admin->show_Admin();

        $this->load->view('admin/edit_admin', $data);
    }

    public function tampildataDosen()
    {

        $data['title'] = 'Edit Dosen';


        $data['dosen'] = $this->m_admin->show_Dosen();

        $this->load->view('admin/edit_dosen', $data);
    }


    public function tampildataMahasiswa()
    {

        $data['title'] = 'Edit Mahasiswa';


        $data['mahasiswa'] = $this->m_admin->show_Mahasiswa();

        $this->load->view('admin/edit_mahasiswa', $data);
    }


    public function tampildataTeknik()
    {

        $data['title'] = 'Fakultas Teknik';


        $data['fakultas_teknik'] = $this->m_admin->show_FakTeknik();

        $this->load->view('admin/edit_FakTeknik', $data);
    }

    public function tampildataPaketMk()
    {

        $data['title'] = 'Data Paket Matakuliah';


        $data['paket_mk'] = $this->m_admin->show_PaketMk();

        $this->load->view('admin/edit_paketMk', $data);
    }

    public function tampildataFakultas()
    {

        $data['title'] = 'Data Fakultas';


        $data['idfak'] = $this->input->get('idfakultas');

        $data['data_prodi'] = $this->m_admin->show_Prodi($data['idfak']);
        $data['data_fakultas'] = $this->m_admin->show_Fakultas();


        $this->load->view('admin/edit_fakultas', $data);
    }

    public function tampildataMk()
    {

        $data['title'] = 'Mata Kuliah';

        $data['mata_kuliah'] = $this->m_admin->show_Mk();

        $this->load->view('admin/edit_matakuliah', $data);
    }

    public function tampildataPaket()
    {

        $data['title'] = 'Data Paket';

        $data['paket_mk'] = $this->m_admin->show_PaketMk();

        $this->load->view('admin/edit_paketMk', $data);
    }

    public function tampildataNilai()
    {

        $data['title'] = 'Data Nilai';

        $data['nilai'] = $this->m_admin->show_Nilai();

        $this->load->view('admin/edit_nilaiMhs', $data);
    }


    public function updateAdmin()
    {

        $id = $this->input->post('id');
        $data['nama'] = $this->input->post('nama');
        $data['username'] = $this->input->post('username');
        $data['email'] = $this->input->post('email');

        $this->m_admin->update_admin($data, $id);

        redirect('Admin/tampildataAdmin');
    }

    public function updateDosen()
    {

        $id = $this->input->post('kd_dosen');
        $data['kd_jurusan'] = $this->input->post('kd_jurusan');
        $data['nama'] = $this->input->post('nama');
        $data['nidn'] = $this->input->post('nidn');
        $data['email'] = $this->input->post('email');
        $data['alamat'] = $this->input->post('alamat');
        $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');

        $this->m_admin->update_dosen($data, $id);

        redirect('Admin/tampildataDosen');
    }

    public function updateMhs()
    {

        $id = $this->input->post('id_mhs');
        $data['nim'] = $this->input->post('nim');
        $data['nama'] = $this->input->post('nama');
        $data['email'] = $this->input->post('email');
        $data['alamat'] = $this->input->post('alamat');
        $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
        $data['tempat_lahir'] = $this->input->post('tempat_lahir');
        $data['tgl_lahir'] = $this->input->post('tgl_lahir');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['agama'] = $this->input->post('agama');
        $data['kewarganegaraan'] = $this->input->post('kewarganegaraan');

        $this->m_admin->update_Mhs($data, $id);

        redirect('Admin/tampildataMahasiswa');
    }


    public function updateMk()
    {

        $id = $this->input->post('kd_mk');
        $data['kd_prodi'] = $this->input->post('kd_prodi');
        $data['matakuliah'] = $this->input->post('matakuliah');
        $data['sks'] = $this->input->post('sks');
        $data['semester'] = $this->input->post('semester');

        $this->m_admin->update_Mk($data, $id);

        redirect('Admin/tampildataMk');
    }

    public function updatePaket()
    {

        $id = $this->input->post('id_paket');
        $data['kd_paket'] = $this->input->post('kd_paket');
        $data['jmlh_mk'] = $this->input->post('jmlh_mk');
        $data['paket_smster'] = $this->input->post('paket_smster');
        $data['tot_sks'] = $this->input->post('tot_sks');
        $data['kd_prodi'] = $this->input->post('kd_prodi');

        $this->m_admin->update_Paket($data, $id);

        redirect('Admin/tampildataPaketMk');
    }

    public function updateNilai()
    {

        $id = $this->input->post('nim');
        $data['tugas'] = $this->input->post('tugas');
        $data['ke_aktifan'] = $this->input->post('ke_aktifan');
        $data['uts'] = $this->input->post('uts');
        $data['uas'] = $this->input->post('uas');

        $this->m_admin->update_Nilai($data, $id);

        redirect('Admin/tampildataNilai');
    }



    public function hapusAdmin($id)
    {

        $this->m_admin->delete($id);

        redirect('Admin/tampildataAdmin');
    }


    public function hapusDosen($id)
    {

        $this->m_admin->deleteDosen($id);

        redirect('Admin/tampildataDosen');
    }

    public function hapusMahasiswa($id)
    {

        $this->m_admin->deleteMhs($id);

        redirect('Admin/tampildataMahasiswa');
    }

    public function hapusMK($id)
    {

        $this->m_admin->deleteMk($id);

        redirect('Admin/tampildataMk');
    }

    public function hapusPaket($id)
    {

        $this->m_admin->deletePaket($id);

        redirect('Admin/tampildataPaket');
    }

    public function hapusNilai($id)
    {

        $this->m_admin->deleteNilai($id);

        redirect('Admin/tampildataNilai');
    }

    public function hapusProdi($id)
    {

        $this->m_admin->deleteProdi($id);

        redirect('Admin/tampildataFakultas');
    }



    public function simpanDosen()
    {
        $data['kd_dosen'] = $this->input->post('id_dosen');
        $data['kd_jurusan'] = $this->input->post('kd_dosen');
        $data['nidn'] = $this->input->post('nidn');
        $data['nama'] = $this->input->post('namadosen');
        $data['alamat'] = $this->input->post('alamatdosen');
        $data['email'] = $this->input->post('emaildosen');
        $data['jenis_kelamin'] = $this->input->post('jnsklmndosen');
        $data['username'] = $this->input->post('usernamedosen');
        $data['password'] = $this->input->post('passdosen');
        $data['role_id'] = $this->input->post('roleid_dosen');

        $this->m_admin->createDosen($data);

        redirect('Admin/tampildataDosen');
    }


    public function simpanAdmin()
    {
        $data['id'] = $this->input->post('id');
        $data['nama'] = $this->input->post('nama');
        $data['email'] = $this->input->post('email');
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['role_id'] = $this->input->post('role_id');

        $this->m_admin->createAdmin($data);

        redirect('Admin/tampildataAdmin');
    }


    public function simpanMhs()
    {

        $data['id_mhs'] = $this->input->post('id_mhs');
        $data['nim'] = $this->input->post('nim');
        $data['nama'] = $this->input->post('nama');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
        $data['tempat_lahir'] = $this->input->post('tempat_lahir');
        $data['kewarganegaraan'] = $this->input->post('kewarganegaraan');
        $data['agama'] = $this->input->post('agama');
        $data['tgl_lahir'] = $this->input->post('tgl_lahir');
        $data['email'] = $this->input->post('email');
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['role_id'] = $this->input->post('role_id');



        // mulai edit dari sini
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = 'assets/'; //string, the default is application/cache/
        $config['errorlog']     = 'assets/'; //string, the default is application/logs/
        $config['imagedir']     = 'assets/images/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = $data['nim'] . '.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = 'Nim : ' . $data['nim'] . "\n" . 'No Telp : ' . $data['no_telp'] . "\n" . 'Tanggal Lahir :' . $data['tgl_lahir'] . "\n" . 'Username : ' . $data['username'] . "\n" . 'Password :' . $data['password'] . "\n"; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        // editan dari kelr disni
        $data['qr_code'] = $params['savename'];

        $this->m_admin->createMhs($data);

        redirect('Admin/tampildataMahasiswa');
    }


    public function simpanMk()
    {
        $data['kd_mk'] = $this->input->post('kd_mk');
        $data['kd_prodi'] = $this->input->post('kd_prodi');
        $data['matakuliah'] = $this->input->post('matakuliah');
        $data['sks'] = $this->input->post('sks');
        $data['semester'] = $this->input->post('semester');

        $this->m_admin->createMk($data);

        redirect('Admin/tampildataMk');
    }

    public function simpanPaket ()
    {
        $data['id_paket'] = $this->input->post('id_paket');
        $data['kd_paket'] = $this->input->post('kd_paket');
        $data['jmlh_mk'] = $this->input->post('jmlh_mk');
        $data['paket_smster'] = $this->input->post('paket_smster');
        $data['tot_sks'] = $this->input->post('tot_sks');
        $data['kd_prodi'] = $this->input->post('kd_prodi');

        $this->m_admin->createPaket($data);

        redirect('Admin/tampildataPaket');
    }



    public function cariFakultas()
    {

        $data = $this->input->get('fakultas');

        $data['hasil'] = $this->m_admin->pencarianFakultas($data)->result_array();
        $this->load->view("admin/cari_fakultas", $data); // ini view menampilkan hasil pencarian

    }

    public function coba_qrcode()
    {

        // use Endroid\QrCode\QrCode;
        $qrCode = new endroid\qrCode\QrCode('Life is too short to be generating QR codes');

        header('Content-Type: ' . $qrCode->getContentType());
        echo $qrCode->writeString();
    }


}
