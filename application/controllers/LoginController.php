<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel', 'User');
    }

    function index()
    {
        $data['title'] = 'Login Akademik';
        $this->load->view('auth/login', $data);
    }

    function login()
    {

        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == TRUE) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $cekAdmin = $this->User->checkLoginAdmin($username, $password);
            $cekDosen = $this->User->checkLoginDosen($username, $password);
            $cekMahasiswa = $this->User->checkLoginMahasiswa($username, $password);

            if ($cekAdmin) {
                $account = $this->User->checkLoginAdmin($username, $password);
                $id = 'id_admin';
            } else if ($cekDosen) {
                $account = $this->User->checkLoginDosen($username, $password);
                $id = 'id_dosen';
            } elseif ($cekMahasiswa) {
                $account = $this->User->checkLoginMahasiswa($username, $password);
                $id = 'id_mahasiswa';
            }



            if (!empty($account)) {
                $sessionData[$id] = $account['id'];
                $sessionData['nim'] = $account['nim'];
                $sessionData['nidn'] = $account['nidn'];
                $sessionData['nama'] = $account['nama'];
                $sessionData['alamat'] = $account['alamat'];
                $sessionData['email'] = $account['email'];
                $sessionData['jenis_kelamin'] = $account['jenis_kelamin'];
                $sessionData['username'] = $account['username'];
                $sessionData['password'] = $account['password'];
                $sessionData['role_id'] = $account['role_id'];
                $sessionData['unique_code'] = $account['unique_code'];
                $sessionData['is_login'] = TRUE;


                $this->session->set_userdata($sessionData);

                if ($this->session->userdata('role_id') == '1') {
                    redirect('Admin/index');
                } else if ($this->session->userdata('role_id') == '2') {
                    redirect('Dosen/index');
                } else if ($this->session->userdata('role_id') == '3') {
                    redirect('Mahasiswa/index');
                }
            } else {
                echo "<script>
                alert('Username/Password Salah');
                </script>";
            }
        }

        $this->load->view('auth/login');
    }

    function logout()
    {

        $this->session->sess_destroy();
        redirect('login');
    }
}

/* End of file LoginAdminController.php */
/* Location: ./application/controllers/LoginAdminController.php */
