<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_dosen');
    }

    public function index()
    {
        $data['title'] = 'Dosen';
        $data['total_matkuldos'] = $this->m_dosen->hitung_matkuldos();
        $data['total_mahasiswados'] = $this->m_dosen->hitung_mahasiswados();
        $this->load->view('dosen/dashboard_dosen', $data);
    }


    public function editUser()
    {
        $data['dosen'] = $this->m_dosen->getDosen();

        $this->load->view('admin/edit_dosen', $data);
    }

    public function tampildataMkdos()
    {

        $data['title'] = 'Mata Kuliah';

        $data['mata_kuliah'] = $this->m_dosen->show_Mkdos();

        $this->load->view('dosen/dosen_edit_matakuliah', $data);
    }

    public function tampildatanilaidos()
    {

        $data['title'] = 'edit nilai';

        $data['edit_nilai'] = $this->m_dosen->show_nilaidos();

        $this->load->view('dosen/dosen_edit_nilai', $data);
    }

    public function updateMkdos()
    {

        $id = $this->input->post('id_nilai');
        $data['tugas'] = $this->input->post('tugas');
        $data['ke_aktifan'] = $this->input->post('ke_aktifan');
        $data['uts'] = $this->input->post('uts');
        $data['uas'] = $this->input->post('uas');

        $this->m_dosen->update_Mkdos($data, $id);

        redirect('Dosen/tampildatanilaidos');
    }
}

