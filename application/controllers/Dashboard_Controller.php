<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_Controller extends CI_Controller
{

    public function index()
    {
        $data['title'] = 'Akademik UHAMKA';
        $this->load->view('dashboard_main', $data);
    }
}
